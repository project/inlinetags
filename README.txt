Module: Inline Tags
Author: Adrian Rollett <http://reluctanthacker.rollett.org/>


Description
===========
Parses free tags placed in the body of enabled node types for a 
selected vocabulary. This is probably most useful for xmlrpc clients,
as there is currently no xmlrpc method that enables free tagging.


Requirements
============
* Taxonomy Module


Installation
============
* Copy the 'inlinetags' module directory in to your Drupal
modules directory. (sites/all/modules)


Usage
=====
In the settings page, (admin/settings/inlinetags) select which
vocabulary you wish to use inlinetags with. Save the settings,
and then select which node types (if multiple types are available)
you would like to enable inline tags for.

There are two behaviors for adding inline tags to nodes - you 
may either have inlinetags overwrite current tags, or not. If
you choose not to overwrite, any tags placed inline will be added
to those already associated with the node.

Once you have inlinetags configured to your liking, any tags in
the following format will be added to nodes when saved.

[tags]tag, some tag, another tag[/tags]


Integrating with an xmlrpc client
=================================
I found it useful to enable the overwrite tags setting for use with
an xmlrpc client - When editing a post, I pull the existing tags with
the mt.getPostCategories method, and pre-populate [tags][/tags] within
the body of the edited post. Then, any changes made during the edit
are saved in a more WYSIWYGish manner.

Credits
=======
The original idea and code came from Sean Buscay, at

http://seanbuscay.com/code/inline-tags-sample-module-code

My contribution was simply to turn his code into a functioning module,
and add the configuration code.
